package cygses

import (
	"crypto/sha512"
	"encoding/hex"
	"net/http"
	"sync"
	"time"
)

type values map[string]string
type sessionStore map[string]sesTable

type sesTable struct {
	remoteAddr string    //IP address of the client
	userAgent  string    //User agent of client (sessions are not valid cross-browser)
	expiration time.Time //Expiration time of session
	valTable   values    //Public session values
	dead       bool      //If the session is sill valid, this is false
	set        bool      //Value set to false by default to prevent clients from creating their own session tokens
}

type sbc struct {
	bin sessionStore //All of the Sessions
	mux sync.Mutex   //Only one session can be accessed at a time
}

var sessionLength time.Duration = 24
var initialized = false
var sessionBin sbc
var scope chan string

//The overseer is a concurrent process who's only job is to kill expired sessions.

func overseer() {
	for {
		//lock the session
		sessionBin.mux.Lock()
		currentString := <-scope //The current CygID
		currentSes := Session(sessionBin.bin[currentString])
		if currentSes.expiration.Before(time.Now()) || currentSes.dead {
			//Delete all references to the session, leave for garbage collection.
			delete(sessionBin.bin, currentString)
		} else {
			//If the session is still valid, add it to the end of the channel to be looked at later
			scope <- currentString
		}
		sessionBin.mux.Unlock()
		//Prevent the process from taking up too much of the CPU.
		time.Sleep(time.Millisecond)
	}
}

func newSession(superString, ra, ua string) http.Cookie {
	//Create the name for the cookie
	cygidHash := sha512.New()
	cygidHash.Write([]byte(superString))
	name := hex.EncodeToString(cygidHash.Sum(nil))

	//Add the sessionStore to the overseer's scope
	//Create a new entry in the SessionBin and initialize sesTable and values
	expiry := time.Now().Add(time.Hour * sessionLength)
	sessionBin.mux.Lock()
	sessionBin.bin[name] = sesTable{
		remoteAddr: ra,
		userAgent:  ua,
		expiration: expiry,
		valTable:   make(map[string]string),
		dead:       false,
		set:        true,
	}
	sessionBin.mux.Unlock()

	//return the sessionStore id cookie
	return http.Cookie{
		Name:    "CYGID",
		Value:   name,
		Expires: expiry,
	}
}

//Unconditionally builds a new session and replaces the CYGID cookie
func buildNewSession(ua, ra string, w http.ResponseWriter) *http.Cookie {
	now := time.Now().UnixNano()
	superString := string(now) + ua + ra
	newCookie := newSession(superString, ra, ua)
	http.SetCookie(w, &newCookie)
	return &newCookie
}

//Validates a session, returns errBadSession if the session is invalid.
func validate(ses Session, r *http.Request) bool {

	//lock the session until it is validated
	sessionBin.mux.Lock()
	defer sessionBin.mux.Unlock()

	//Validate the client has the same IP address as the session
	if r.RemoteAddr != ses.remoteAddr {
		return false
	}

	//Validate the client is using the same browser as the session
	if r.UserAgent() != ses.userAgent {
		return false
	}

	//Validate the session is still alive
	if ses.dead {
		return false
	}

	//Validate the session is not a fake session token
	if !ses.set {
		return false
	}

	//Validate that the session had not expired
	if ses.expiration.Before(time.Now()) {
		//Setting the session as dead makes it easier for the overseer to remove it.
		ses.dead = true
		return false
	}

	//The session has passed all tests, it is valid
	return true
}
