package cygses

import (
	"log"
	"net/http"
	"time"
)

//Set the default time period that an initiated sessionStore should be kept alive for
//in ms.
func SetDefaultLength(length time.Duration) {

	sessionLength = length
}

//Returns a Session that contains information on user sessions.
func GetSession(w http.ResponseWriter, r *http.Request) Session {
	ua := r.Header.Get("User-Agent")
	ra := r.RemoteAddr

	//if GetSession is running for the first time...
	//This is mutex locked so that we don't run 2 concurrent instances of overseer
	sessionBin.mux.Lock()
	if !initialized {
		//start the sessionStore overseer
		sessionBin.bin = make(map[string]sesTable)
		scope = make(chan string)
		initialized = true
		go overseer()
	}
	sessionBin.mux.Unlock()

	//Retrieve the sessionStore ID
	cookie, e := r.Cookie("CYGID")

	//If the cookie isn't set, create a new sessionStore
	if e == http.ErrNoCookie {
		cookie = buildNewSession(ua, ra, w)
	}

	//validate the session
	sessionBin.mux.Lock()
	ses := Session(sessionBin.bin[cookie.Value])
	sessionBin.mux.Unlock()
	valid := validate(ses, r)
	if !valid {
		//If the session is invalid, build a new one.
		cookie = buildNewSession(ua, ra, w)
		sessionBin.mux.Lock()
		ses = Session(sessionBin.bin[cookie.Value])
		sessionBin.mux.Unlock()
		log.Println("Session " + cookie.Value + " was replaced because the session token was invalid.")
	}

	//return the Session.
	return ses
}
