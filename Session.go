package cygses

type Session sesTable

//Returns specified session value
func (ses Session) Get(s string) string {
	sessionBin.mux.Lock()
	defer sessionBin.mux.Unlock()
	return ses.valTable[s]
}

//Sets a session value
func (ses Session) Set(string, value string) {
	sessionBin.mux.Lock()
	defer sessionBin.mux.Unlock()
	ses.valTable[string] = value
}

//Kills a session
func (ses Session) Kill(s string) {
	sessionBin.mux.Lock()
	defer sessionBin.mux.Unlock()
	ses.dead = true
}
